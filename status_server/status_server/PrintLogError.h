//
//  PrinLogError.h
//  status_server
//
//  Created by Liyuan Liu on 2/22/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef PrinLogError_h
#define PrinLogError_h

enum PrintLogError
{
    OpenFileFailed =    -1,
    CloseFileFailed =   -2,
    WriteFileFailed =   -3
};

#define APPEND      10 // attache new content to log
#define OVERWRITE   20 // overi write new content to log
#endif /* PrinLogError_h */
