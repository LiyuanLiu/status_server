//
//  common_macro.h
//  status_server
//
//  Created by Liyuan Liu on 2/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef common_macro_h
#define common_macro_h

//------------status of user ---------------------
#define AVALIABLE               "300" //anyone can call this user
#define NOT_AVALIABLE           "301"//no one can call this user
#define BUSY                    "302"//don't call him unless you have important issues

#define CUSTOMIZED_STATUS       "400"//user decides their particular status


//macro
#define INIT_VALUE              "#"
#define SEPERATOR               "|"

//---------------length-------------------------------

#define REQUEST_TYPE_LENGTH_MAX         4
#define REPLY_TYPE_LENGTH_MAX           4
#define NOTICE_TYPE_LENGTH_MAX          4
#define ANSWER_TYPE_LENGTH_MAX          4
#define AGE_LENGTH                      4

#define LENGTH_STRING_MAX               10//When send the strin of length, the max size of the string of length
#define BIRTHDAY_LENGTH                 12
#define PASSWORD_MAX                    20
#define PHONE_NUM_LENGTH                20
#define LOCATION_LENGTH                 50
#define USERNAME_MAX                    50
#define SCHEDULE_NAME_MAX               50

//belong to work profile
#define COMPANY_NAME_MAX                50
#define POSITION_NAME_MAX               50
//belong to edu profile
#define SCHOOLE_NAME_MAX                50
#define MAJOR_NAME_MAX                  50

#define POLICY_NAME_MAX                 50
#define EMAIL_ADDR_MAX                  100
#define CUSTOMIZED_STATUS_MAX           150
#define CHATTING_MSG_MAX                1024
#endif /* common_macro_h */














