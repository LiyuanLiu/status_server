//
//  TCPConnect.cpp
//  status_server
//
//  Created by Liyuan Liu on 2/11/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#include "TCPConnect.hpp"
#include <stdexcept>

TCPConnect::TCPConnect(int MaxCon, char * filename)
{
    MaxConnectionNum = MaxCon;
    logName = (char *)malloc(strlen(filename)+1);
    strcpy(logName, filename);
    total_num_threads = 0;
    
}

int TCPConnect::ConnectRequest(char *ip, int port,void * PrintLogfunc(char*,char*,int))
{
    if((SocketToServer=socket(AF_INET, SOCK_STREAM, 0))==-1)
    {
        printf("establish sockfd to server failed:%d\n",errno);
        char *temp;
        sprintf(temp, "establish sockfd to server failed:%d",errno);
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        
        
        client_connect_errno = CREATE_SOCK_TO_SERVER_FAILED;
        return client_connect_errno;
    }
    struct sockaddr_in ts_socketAddrToServer;
    SetupSocketAddrToTarget(ip, port,ts_socketAddrToServer);
    if(connect(SocketToServer, (struct sockaddr*)&ts_socketAddrToServer, sizeof(struct sockaddr))<0)
    {
        printf("connect to server failed:%d\n",errno);
        char *temp;
        sprintf(temp, "connect to server failed:%d",errno);
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        client_connect_errno = CONNECT_SERVER_FAILED;
        return client_connect_errno;
    }
    return 0;
    
}

int TCPConnect::SetupSocketAddrToTarget(char * ip,int port,struct sockaddr_in &sockaddr,void * PrintLogfunc(char*,char*,int))
{
    
    
    struct hostent * host = gethostbyname(ip);
    if(host == NULL)
    {
        printf("cannot get host by name:%d\n",errno);
        char *temp;
        sprintf(temp, "cannot get host by name:%d",errno);
        
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        
        client_connect_errno = GETHOSTBYNAME_FAILED;
        
        return client_connect_errno;
    }
    //char **p;
    switch (host->h_addrtype)
    {
        case AF_INET:
            sockaddr.sin_family = AF_INET;
            sockaddr.sin_port = htons(port);
            memcpy(&sockaddr.sin_addr.s_addr, host->h_addr, host->h_length);
            
            break;
            
        case AF_INET6:
            sockaddr.sin_family = AF_INET6;
            sockaddr.sin_port = htons(port);
            memcpy(&sockaddr.sin_addr.s_addr, host->h_addr, host->h_length);
            break;
            
        default:
            break;
    }
    return 0;
    
}
int TCPConnect::StartUpListenBind(int port,std::function<int(int,struct sockaddr_in)> server_process)
{
    if((SocketToListen=socket(AF_INET, SOCK_STREAM, 0))==-1)
    {
        printf("create server socket failed:%d\n",errno);
        
        char *temp;
        sprintf(temp, "create server socket failed:%d",errno);
        
            printLog.PrintLogToFile(logName, temp,APPEND);
       
        server_connect_errno = CREATE_SOCK_FAILED;
        return server_connect_errno;
    }
    
    struct sockaddr_in ts_SocketAddrToListen;
    ts_SocketAddrToListen.sin_addr.s_addr = INADDR_ANY;
    ts_SocketAddrToListen.sin_family = AF_INET;
    ts_SocketAddrToListen.sin_port = htons(port);
    
    if (::bind(SocketToListen, (struct sockaddr *) &ts_SocketAddrToListen, sizeof(ts_SocketAddrToListen)) < 0)
    {
        printf("bind failed:%d\n",errno);
        char *temp;
        sprintf(temp, "bind failed:%d",errno);
        
            printLog.PrintLogToFile(logName, temp,APPEND);
       
        server_connect_errno = BIND_FAILED;
        return server_connect_errno;

    }
    if(listen(SocketToListen, 99)==-1)
    {
        printf("listen failed:%d\n",errno);
        char *temp;
        sprintf(temp, "listen failed:%d",errno);
        
            printLog.PrintLogToFile(logName, temp,APPEND);
       
        server_connect_errno = LISTEN_FAILED;
        return server_connect_errno;
        
    }
    
    while (1)
    {
        int client_sock;
        struct sockaddr_in client_sockaddr;
        socklen_t ti_size = sizeof(struct sockaddr_in);
        if((client_sock = accept(SocketToListen, (struct sockaddr *)&client_sockaddr, &ti_size))==-1)
        {
            printf("accept user failed:%d\n",errno);
            char *temp;
            sprintf(temp, "accept user failed:%d",errno);
            
                printLog.PrintLogToFile(logName, temp,APPEND);
           
            server_connect_errno = ACCPET_FAILED;
            return server_connect_errno;

            
        }
        if(total_num_threads < MaxConnectionNum)
        {
            //create thread
            
            total_num_threads++;
            
        }
        else
        {
            //refuse to accept more users
        }
        
    }
    
    
    return 0;
    
}



//length is the length of the msg we want to send
int TCPConnect::SendMsgToServer(char *msg,int length,void * PrintLogfunc(char*,char*,int))
{
    if(SEND_FAILED == SendMsgToClient(SocketToServer, msg, length,PrintLogfunc))
    {
        printf("send to server failed:%d\n",errno);
        char *temp;
        sprintf(temp, "send to server failed:%d",errno);
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        client_connect_errno = SEND_TO_SERVER_FAILED;
        return client_connect_errno;
        
    }
    return 0;
}

int TCPConnect::SendMsgToClient(int sockfd,char *msg,int length,void * PrintLogfunc(char*,char*,int))
{
    
    int len = send(sockfd,msg,length,0);
    if(len==-1)
    {
        printf("send to client failed:%d\n",errno);
        char *temp;
        sprintf(temp, "send to client failed:%d",errno);
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        server_connect_errno = SEND_FAILED;
        return server_connect_errno;
        
    }
    return 0;
    
}

int TCPConnect::CloseConnect(int socketfd,void * PrintLogfunc(char*,char*,int))
{
    if(close(socketfd)==-1)
    {
        printf("failed to close socket:%d\n",errno);
        char *temp;
        sprintf(temp, "failed to close socket:%d",errno);
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        server_connect_errno = CLOSE_FAILED;
        return server_connect_errno;
        
    }
    return 0;
    
}

//this func needs to be removed
/*sockaddr_in TCPConnect::GetInfoBySocket(int sockfd,void * PrintLogfunc(char*,char*,int))
{
    
    return clients[sockfd];
    
}*/

char* TCPConnect::ReceiveFromClient(int socketfd,int nextlen,void * PrintLogfunc(char*,char*,int))
{
    char *msg = new char[nextlen];
    memset(msg, 0, sizeof(char)*nextlen);
    
    int len = 0;
    
    if((len=recv(socketfd,msg,sizeof(char)*nextlen,0))==-1)
    {
        printf("recv from client failed:%d\n",errno);
        char *temp;
        sprintf(temp, "recv from client failed:%d",errno);
        if (PrintLogfunc == NULL)
        {
            printLog.PrintLogToFile(logName, temp,APPEND);
        }
        else
        {
            PrintLogfunc(logName,temp,APPEND);
        }
        return (char*)"recv_error";
        
    }

    
    return msg;
    
}

int TCPConnect::GetNextlen(int sockfd)
{
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(sockfd,&readfds);
    
    int ret = 0;
    ret = select(sockfd+1, &readfds, NULL, NULL, NULL);
    
    if(FD_ISSET(sockfd,&readfds))
    {
        int nextlen = 0;
        int ret_ioctl = 0;
        ret_ioctl = ioctl(sockfd, FIONREAD,&nextlen);
        return nextlen;
    }
    return 0;
}

char* TCPConnect::ReceiverFromServer(int nextlen)
{
    return ReceiveFromClient(SocketToServer, nextlen);
}












