//
//  server.cpp
//  status_server
//
//  Created by Liyuan Liu on 2/22/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#include "server.hpp"


Server::Server(int maxconnect,char *fname)
{
    serverTCPConnect.MaxConnectionNum = maxconnect;
    strcpy(serverTCPConnect.logName, fname);
    
}

int Server::Server_Process_Users(void *arg)
{
    thread_arg pass_arg = *((thread_arg *)arg);
    
    client_data user;
    time(&user.alive_time);
    user.addr = pass_arg.addr;
    
    clients_online.insert ( std::pair<int,client_data>(pass_arg.sock,user));
    
#ifdef LONG_ON
    long_header header_check;
    memset(&header_check, 0, sizeof(long_header));
#else
    default_short_header header_check;
    memset(&header_check, 0, sizeof(default_short_header));
#endif
    while (1)
    {
        char *msg;
        int nextlen = serverTCPConnect.GetNextlen(pass_arg.sock);
        msg = (char *)malloc(sizeof(char)*nextlen);
        memset(msg,0,sizeof(char)*nextlen);
        
        msg = serverTCPConnect.ReceiveFromClient(pass_arg.sock, nextlen);
        
#ifdef LONG_ON
        memcpy(&header_check, msg, sizeof(long_header));
#else
        memcpy(&header_check, msg, sizeof(default_short_header));
#endif
        if (strcmp(header_check.request_type, REGISTER)==0 )
        {
            int err =0;
            err = Register(pass_arg.sock, msg);
        }
        else if(strcmp(header_check.request_type, LOGIN)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, LOGOUT)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, UPDATE_POLICY)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, UPDATE_SCHEDULE)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, UPDATE_PROFILE)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, SEARCH_FRIEND)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, ADD_FRIEND)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, DELETE_FRIEND)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, CHAT)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, IGNORE_ADD_REQUEST)==0)
        {
            
        }
        else if (strcmp(header_check.request_type, REJECT_ADD_REQUEST)==0)
        {
            
        }
        
    }
    
    
    
    
    
    // when logout, or client doesn't response, pthread_exit(0); and serverTCPConnect.total_num_threads--
    
    return 0;
}
int Server::Server_Start()
{
    
    
   // std:: function<int(int,struct sockaddr_in)> test = std::bind(&Server::Server_Process_Users, this, -1);
    
    //serverTCPConnect.StartUpListenBind(8000,std::bind(&Server::Server_Process_Users));
    
    return 0;
}


int Server::Register(int sock, char *msg)
{
    request_reg reg;
    memset(&reg,0,sizeof(request_reg));
    memcpy(&reg, msg, sizeof(request_reg));
    
    //sql search this username,if not exists, insert  it and send reg succeed to client
    //otherwise, sends failed
    return 0;
}

int Server::Login(int sock, char *msg)
{
    request_login login;
    memset(&login,0,sizeof(request_login));
    memcpy(&login, msg, sizeof(request_login));
    
    
    
    return 0;
}

int Server::Logout(int sock, char *msg)
{
    request_logout logout;
    memset(&logout,0,sizeof(request_logout));
    memcpy(&logout, msg, sizeof(request_logout));
    
    
    return 0;
}
int Server::Update_Policy(int sock, char *msg)
{
    request_update_policy policy;
    memset(&policy,0,sizeof(request_update_policy));
    memcpy(&policy, msg,sizeof(request_update_policy));
    
    
    return 0;
}

int Server::Update_Schedule(int sock, char *msg)
{
    request_update_schedule schedule;
    memset(&schedule, 0, sizeof(request_update_schedule));
    memcpy(&schedule, msg, sizeof(request_update_schedule));
    
    
    return 0;
}

int Server::Update_Profile(int sock, char *msg)
{
    request_update_profile profile;
    memset(&profile, 0, sizeof(request_update_profile));
    memcpy(&profile, msg, sizeof(request_update_profile));
    
    
    return 0;
}

int Server::Search_Friend(int sock, char *msg)
{
    request_search_friend search_friend;
    memset(&search_friend,0,sizeof(request_search_friend));
    memcpy(&search_friend, msg, sizeof(request_search_friend));
    
    return 0;
}

int Server::Add_Friend(int sock, char *msg)
{
    request_add_friend add_friend;
    memset(&add_friend, 0, sizeof(request_add_friend));
    memcpy(&add_friend, msg, sizeof(request_add_friend));
    
    return 0;
}

int Server::Delete_Friend(int sock, char *msg)
{
    request_del_friend del_friend;
    memset(&del_friend, 0, sizeof(request_del_friend));
    memcpy(&del_friend, msg, sizeof(request_del_friend));
    
    return 0;
}

int Server::Chat(int sock, char *msg)
{
    request_chat chat;
    memset(&chat,0,sizeof(request_chat));
    memcpy(&chat, msg, sizeof(request_chat));
    
    return 0;
}

int Server::Igore_Add_Request(int sock, char *msg)
{
    short_answer_header ignore;
    memset(&ignore, 0, sizeof(short_answer_header));
    memcpy(&ignore, msg, sizeof(short_answer_header));
    
    
    return 0;
}

int Server::Reject_Add_Request(int sock, char *msg)
{
    short_answer_header reject;
    memset(&reject, 0, sizeof(short_answer_header));
    memcpy(&reject, msg, sizeof(short_answer_header));
    
    return 0;
}


















