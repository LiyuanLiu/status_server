//
//  TCPConnectionError.h
//  status_server
//
//  Created by Liyuan Liu on 2/12/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef TCPConnectionError_h
#define TCPConnectionError_h


//----------errno on TCPConnect on Server side----------
enum server_TCP_errno
{
    CREATE_SOCK_FAILED =    -1,
    BIND_FAILED =           -2,
    LISTEN_FAILED =         -3,
    ACCPET_FAILED =         -4,
    RECV_FAILED =           -5,
    SEND_FAILED =           -6,
    CLOSE_FAILED =          -7
    
    
};


//----------errno on TCPConnect on Client side-----------
enum client_TCP_errno
{
    CONNECT_SERVER_FAILED =         -8,
    CREATE_SOCK_TO_SERVER_FAILED =  -9, 
    GETHOSTBYNAME_FAILED =          -10,
    SEND_TO_SERVER_FAILED =         -11
    
};


#endif /* TCPConnectionError_h */
