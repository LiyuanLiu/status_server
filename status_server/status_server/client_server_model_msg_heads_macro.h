//
//  client_server_model_msg_heads_macro.hpp
//  status_server
//
//  Created by Liyuan Liu on 2/7/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//




/*
 This is used to define macro about the msg heads to 
 sent between client and server
 */
#ifndef client_server_model_msg_heads_macro_hpp
#define client_server_model_msg_heads_macro_hpp

#include <stdio.h>


//------------client send to server(positively)------------------------------------------------
#define REGISTER            "10"
#define LOGIN               "20"
#define LOGOUT              "30"

//---send filtering policy-------
#define UPDATE_POLICY       "40"
//ask for a specific policy
#define ASK_POLICY          "41"
//ask total policy name
#define ASK_ALL_POLICY      "42"
//add new policy
#define ADD_NEW_POLICY      "43"


//----send my schedule-----------
#define UPDATE_SCHEDULE     "50"
//ask for a specific schdeule
#define ASK_SCHEDULE        "51"
//ask for all schedule
#define ASK_ALL_SCHEDULE    "52"
//add new schedule
#define ADD_NEW_SCHEDULE    "53"


//----send my profile------------
#define UPDATE_PROFILE      "55"
//ask for work profile
#define ASK_WORK_PROFILE    "56"
//ask for basic profile
#define ASK_BASIC_PROFILE   "57"
//ask for edu profile
#define ASK_EDU_PROFILE     "58"


//----search friends-------------
#define SEARCH_FRIEND       "60"

//----add friends----------------
#define ADD_FRIEND          "65"

//----delete friends-------------
#define DELETE_FRIEND       "70"

//---send chatting msg-----------
#define CHAT                "75"

//------online automatically-----
//client will send update msg to server, otherwise server will view client as logout due to client crash
#define ONLINE_UPDATE       "80"

//-------------server replies to client(negatively)--------------------------------------------
//----succeed-----------------------------------------------------
#define REGISTER_SUCCEED                "110"
#define LOGIN_SUCCEED                   "120"
#define LOGOUT_SUCCEED                  "130"

#define UPDATE_POLICY_SUCCEED           "140"
#define REPLY_POLICY                    "141"
#define REPLY_ALL_POLICY                "142"

#define UPDATE_SCHEDULE_SUCCEED         "150"
#define REPLY_SCHEDULE                  "151"
#define REPLY_ALL_SCHEDULE              "152"

#define UPDATE_PROFILE_SUCCEED          "155"
#define REPLY_WORK_PROFILE              "156"
#define REPLY_BASIC_PROFILE             "157"
#define REPLY_EDU_PROFILE               "158"

#define SEARCH_RESULT                   "160"
#define ADD_SUCCEED                     "165"
#define DELETE_FRIEND_SUCCEED           "170"
#define CHAT_SUCCEED                    "175"
#define ONLINE_UPDATE_SUCCEED           "180"

//------failed------------------------------------------------------
//--------register failed-------------------------
#define REGISTER_FAILED_USERNAME_EXIST      "-10"
//--------------LOGIN FAILED----------------------
#define LOGIN_FAILED_USERNAME_ONLINE        "-20"
#define LOGIN_FAILED_USERNAME_NOT_EXIST     "-21"
#define LOGIN_FAILED_PASSWORD_ERROR         "-22"

//-----logout failed------------------------------
#define LOGOUT_FAILED_NOT_ONLINE            "-30"
#define LOGOUT_FAILED_NO_USERNAME           "-31"



/*
 1,limit someone can search my account just by my account name
 2,group friends in different groups, some groups can see my one status, some groups can see my 
 another status
 3,
 */
//----update policy failed------------------------
#define UPDATE_POLICY_FAILED                "-40"


/* 1, we can have different versions of schedule, for different groups
 2,we can have different versions of schedule for different days in one week
 3,we can set detailed schedule for each day, and also set a rough schedule 
 for a long time(like will be very busy in Jan)
 */
//-----update schedule failed---------------------
#define UPDATE_SCHEDULE_FAILED              "-50"

//------update profile failed---------------------
#define UPDATE_PROFILE_FAILED               "-55"

//------search friends failed---------------------
#define SEARCH_FRIEND_NOT_EXIST             "-60"

//------add friends failed------------------------
#define ADD_FAILED_REJECT                   "-65"

//------delete friends failed---------------------
#define DELETE_FRIEND_FAILED                "-70"

//------send chat msg failed----------------------
#define CHAT_LOGOUT_FAILED                  "-75"

//-------online update failed---------------------
#define ONLINE_UPDATE_FAILED                "-80"



//-----------------server sends to client(positively)------------------------------------------------
//--------server tells client that anther wants to add him-----------
#define ADD_YOU                              "85"
//--------server tells client that another sends him chatting msg----
#define CHAT_TO_YOU                          "90"


//-----------------client replies to server(negatively)----------------------------------------------
#define IGNORE_ADD_REQUEST                  "566"
#define REJECT_ADD_REQUEST                  "567"



#endif /* client_server_model_msg_heads_macro_hpp */


















