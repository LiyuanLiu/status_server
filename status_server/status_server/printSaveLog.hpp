//
//  printSaveLog.hpp
//  status_server
//
//  Created by Liyuan Liu on 2/22/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef printSaveLog_hpp
#define printSaveLog_hpp

#include <stdio.h>
#include <time.h>
#include "PrintLogError.h"


class PrintSaveLog
{
protected:
    PrintLogError LogFileError;
    
    
public:
    int PrintLogToFile(char *fname, char * content, int over_append = APPEND);
    
};

#endif /* printSaveLog_hpp */
