//
//  database_err_succeed_macro.h
//  status_server
//
//  Created by Liyuan Liu on 2/27/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef database_err_succeed_macro_h
#define database_err_succeed_macro_h


#define INSERT_ERROR            -1
#define DELETE_ERROR            -2
#define EDIT_ERROR              -3  //edit it's value in databsed
#define SEARCH_ERROR            -4 //nothing found


#define INSERT_SUCCEED          1
#define DELETE_SUCCEED          2
#define EDIT_SUCCEED            3


#define GROUP_NUM_MAX           100
#define SCHEDULE_NUM_MAX        100


#define GROUP_DEAULT_VALUE      1
#define SCHEDULE_DEFAULT_VALUE  1

#define PERSONAL_INFO           1




#endif /* database_err_succeed_macro_h */
