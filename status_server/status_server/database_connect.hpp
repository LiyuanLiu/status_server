//
//  database_connect.hpp
//  status_server
//
//  Created by Liyuan Liu on 2/24/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef database_connect_hpp
#define database_connect_hpp

#include <stdio.h>
#include "database_err_succeed_macro.h"

//username,pass, basic_info(age,born date, gender,signature,phone num,email_addr,hometown),
//current location
//

class _user_database
{
public:
    int insert();
    int delete_record();
    int search();
    int edit();
    
};


/*database:
 group friend
 friend
 group->different policy
 group->different schedule
 used plicy
 used schedule
 email
 pssword
 */

#endif /* database_connect_hpp */
