//
//  printSaveLog.cpp
//  status_server
//
//  Created by Liyuan Liu on 2/22/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#include "printSaveLog.hpp"


int PrintSaveLog::PrintLogToFile(char *fname,char *content, int over_append)
{
    FILE *fd;
    
    if (over_append == APPEND)
    {
        fd = fopen(fname, "ab");
    }
    else if (over_append == OVERWRITE)
    {
        fd = fopen(fname, "w");
    }
    
    
    if(fd == NULL)
    {
        LogFileError = OpenFileFailed;
        return LogFileError;
    }
    time_t print_time;
    char current_time[80];
    struct tm tstruct;
    print_time = time(0);
    tstruct = *localtime(&print_time);
    strftime(current_time, sizeof(current_time), "%Y-%m-%d.%X", &tstruct);
    fprintf(fd, "%s\n:",current_time);
    fprintf(fd,"%s\n" ,content);
    
    fclose(fd);
    return 0;
}