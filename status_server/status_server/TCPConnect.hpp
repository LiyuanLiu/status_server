//
//  TCPConnect.hpp
//  status_server
//
//  Created by Liyuan Liu on 2/11/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef TCPConnect_hpp
#define TCPConnect_hpp


#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <cerrno>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <iostream>
#include<functional>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "TCPConnectionError.h"
#include "PrintLogError.h"
#include "printSaveLog.hpp"
using namespace::std;

class TCPConnect
{
public:
    int total_num_threads;//record current existing num of threads
    int MaxConnectionNum;
    char * logName;
    
protected:
    int SocketToServer;//as client,establish a socket to server
    int SocketToListen;//as server, establish a socket to client
    
    PrintSaveLog printLog;
   
    server_TCP_errno server_connect_errno;
    client_TCP_errno client_connect_errno;
    
    
protected:
    int SetupSocketAddrToTarget(char * ip,int port,struct sockaddr_in &sockaddr,void * PrintLogfunc(char*,char*,int) = NULL);
    
public:
    TCPConnect(int MaxConnection = 2000, char * filename = (char *)"server.log");
    
    char * ReceiverFromServer(int nextlen);
    char * ReceiveFromClient(int socketfd,int nextlen,void * PrintLogfunc(char*,char*,int) = NULL);
    int ConnectRequest(char *ip, int port,void * PrintLogfunc(char*,char*,int) = NULL);//setup connect to server
    int SendMsgToServer(char *msg,int length,void * PrintLogfunc(char*,char*,int) = NULL);
    int SendMsgToClient(int sockfd,char *msg, int length,void * PrintLogfunc(char*,char*,int) = NULL);
    
    
    int StartUpListenBind(int port,std::function<int(int,struct sockaddr_in)> server_process);
    
    int CloseConnect(int socketfd,void * PrintLogfunc(char*,char*,int) = NULL);
    int GetNextlen(int socketfd);
  
    
};

#endif /* TCPConnect_hpp */































