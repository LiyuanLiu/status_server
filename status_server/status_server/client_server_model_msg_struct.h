//
//  client_server_model_msg_struct.h
//  status_server
//
//  Created by Liyuan Liu on 2/8/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef client_server_model_msg_struct_h
#define client_server_model_msg_struct_h

#include<iostream>
#include "common_macro.h"
#include "client_server_model_msg_heads_macro.h"
#define packed __attribute__((packed))

//----------------------------------------------client to server postively---------------------------------------------

//-------These are structure that client send to server(short msg,to save money)----------------

/*
 
 default msg structure
 
 |------------default short message--------|
 
 |<-----header------>|<-------body-------->|
 
 |request_type | length |   message body   |
 
 |<--4(char)-->|10(char)|<-unknown length->|
 
 
*/




/*shorter msg can save money.If no accidences happen, client will send short msg with default short head.
 Otherwise, in the future,if client needs to use special bits in the msg, just add that
 */
typedef struct _default_short_header
{
    char request_type[REQUEST_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];//the length of the whole msg
    
}default_short_header,request_online_update,request_logout,request_all_policy,request_all_schedule,request_work_profile,request_basic_profile,request_edu_profile;


typedef struct _request
{
    default_short_header head;
    char username[USERNAME_MAX];
    char password[PASSWORD_MAX];
    char email[EMAIL_ADDR_MAX];//this will be null when it is login request
    
}request_login,request_reg;

typedef struct _request_command
{
    default_short_header head;
    char customed_status[CUSTOMIZED_STATUS_MAX];//if it is not customed_status, this part will be null
    
}request_change_status;


//each policy and schedule will have its own index on server client will tell the server that which policy or
//schedule has been changed
typedef struct  _request_update
{
    default_short_header head;
    //this part resevred for update msg
}request_update_policy,request_update_schedule,request_update_profile,request_oneline_update;

typedef struct _request_friend
{
    default_short_header head;
    char friend_name[USERNAME_MAX];
}request_search_friend,request_add_friend,request_del_friend;

typedef struct _request_chat
{
    default_short_header head;
    char friend_name[USERNAME_MAX];
    char msg[CHATTING_MSG_MAX];
}request_chat;

typedef struct _request_specific
{
    default_short_header head;
    char name[SCHEDULE_NAME_MAX];
}request_policy,request_schedule;





//-------These are structure that client send to server(long head, saved for future special usage)----------------

/*
|-----------------------------------------long message structure----------------------------------------|
 
|<-------------------------------------header----------------------------------->|<-------body--------->|
 
|request_type | length |first_reserved_flag |second_reserved_flag|third_reserved_flag|   message body   |
 
|<--4(char)-->|10(char)|<------4(char)----->|<------4(char)----->|<-------4(char)--->|<-unknown length->|

*/


/*shorter msg can save money.If no accidences happen, client will send short msg with default short head.
 Otherwise, in the future,if client needs to use special bits in the msg, just add that
*/
typedef struct _long_header
{
    char request_type[REQUEST_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];//the length of the whole msg. If length string contains charactor '-', it means this is long header.The string after '-' means the real length of this msg
    char first_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char second_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char third_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    
}long_header,long_request_online_update,long_request_logout;



typedef struct _request_start
{
    long_header head;
    char username[USERNAME_MAX];
    char password[PASSWORD_MAX];
    char email[EMAIL_ADDR_MAX];//this will be null when it is login request
    
}long_request_login,long_request_reg;

typedef struct _long_request_command
{
    long_header head;
    char customed_status[CUSTOMIZED_STATUS_MAX];//if it is not customed_status, this part will be null
    
}long_request_change_status;

typedef struct  _long_request_update
{
    long_header head;
    //this part resevred for update msg
}long_request_update_policy,long_request_update_schedule,long_request_update_profile,long_request_oneline_update;

typedef struct _long_request_friend
{
    long_header head;
    char friend_name[USERNAME_MAX];
}long_request_search_friend,long_request_add_friend,long_request_del_friend;

typedef struct _long_request_chat
{
    default_short_header head;
    char friend_name[USERNAME_MAX];
    char msg[CHATTING_MSG_MAX];
}long_request_chat;

//------------------server replies to client negatively-----------------------------------------------------------

//these are default short structure come from server to client--------------

/*
 
 default  reply short msg structure
 
 |--------default short reply message------|
 
 |<-------header------->|<-------body----->|
 
 | reply_type  | length |   message body   |
 
 |<--4(char)-->|10(char)|<-unknown length->|
 
 
 */

typedef struct _reply_short_header
{
    char reply_type[REPLY_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];
    
    
}reply_short_header,default_server_reply_short;

typedef struct _reply_all//reply for all policy or schedule
{
    reply_short_header head;
    int num;
    //char all schedule_name or all policy name
    
}reply_all_policy,reply_all_schedule;

typedef struct _reply_work_profile
{
    reply_short_header head;
    char company_name[COMPANY_NAME_MAX];
    char position_name[POSITION_NAME_MAX];
    char starttime;//its length is reserved for later
    char endtime;
    
}reply_work_profile;

typedef struct _reply_basic_profile
{
    reply_short_header head;
    char age[AGE_LENGTH];
    char location[LOCATION_LENGTH];
    int  gender;
    char phone_num[PHONE_NUM_LENGTH];
    char birthday[BIRTHDAY_LENGTH];
    
}reply_basic_profile;

typedef struct _reply_edu_profile
{
    reply_short_header head;
    char junior_school[SCHOOLE_NAME_MAX];
    char j_from[BIRTHDAY_LENGTH];
    char j_to[BIRTHDAY_LENGTH];
    
    char high_school[SCHOOLE_NAME_MAX];
    char h_from[BIRTHDAY_LENGTH];
    char h_to[BIRTHDAY_LENGTH];
    
    char college_school[SCHOOLE_NAME_MAX];
    char c_from[BIRTHDAY_LENGTH];
    char c_to[BIRTHDAY_LENGTH];
    
    char grad_school[SCHOOLE_NAME_MAX];
    char g_from[BIRTHDAY_LENGTH];
    char g_to[BIRTHDAY_LENGTH];
    
    char major_college[MAJOR_NAME_MAX];
    char major_grad[MAJOR_NAME_MAX];
    
    
}reply_edu_profile;

//-----these are long structure come from server to client--------------

/*
 
 reply long msg structure
 
 |-----------------------------------------long reply structure-----------------------------------------|
 
 |<-------------------------------------header-------------------------------------->|<-------body----->|
 
 |reply_type  | length |first_reserved_flag |second_reserved_flag|third_reserved_flag|   message body   |
 
 |<--4(char)->|10(char)|<------4(char)----->|<------4(char)----->|<-------4(char)--->|<-unknown length->|
 
 */

typedef struct _reply_short_header_l
{
    char reply_type[REPLY_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];
    char first_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char second_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char third_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    
    
}reply_long_header,server_reply_long;


//-------------server send to client postively(default short msg)--------------
typedef struct _notice_short_header
{
    char notice_type[NOTICE_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];
    
}short_notice_header;

typedef struct _short_add_you
{
    short_notice_header head;
    char sender_name[USERNAME_MAX];
}short_add_you;

typedef struct _short_chat_to_you
{
    short_notice_header head;
    char sender_name[USERNAME_MAX];
    char chat_masg[CHATTING_MSG_MAX];
}short_chat;

//-------------server send to client postively(long msg)--------------
typedef struct _notice_long_header
{
    char notice_type[NOTICE_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];
    char first_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char second_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char third_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    
}long_notice_header;

typedef struct _long_add_you
{
    long_notice_header head;
    char sender_name[USERNAME_MAX];
}long_add_you;

typedef struct _long_chat_to_you
{
    long_notice_header head;
    char sender_name[USERNAME_MAX];
    char chat_masg[CHATTING_MSG_MAX];
}long_chat;

//-----------------client replies to server(negatively)-------------------------------
typedef struct _answer_short_header
{
    char notice_type[ANSWER_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];
    
}short_answer_header;

typedef struct _answer_long_header_
{
    char notice_type[NOTICE_TYPE_LENGTH_MAX];
    char length[LENGTH_STRING_MAX];
    char first_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char second_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    char third_reserved_flag[REQUEST_TYPE_LENGTH_MAX];//reserved for future usage
    
}longer_notice_header;


#endif /* client_server_model_msg_struct_h */


































