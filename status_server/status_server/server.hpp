//
//  server.hpp
//  status_server
//
//  Created by Liyuan Liu on 2/22/16.
//  Copyright © 2016 Liyuan Liu. All rights reserved.
//

#ifndef server_hpp
#define server_hpp

#include <stdio.h>
#include "TCPConnect.hpp"
#include "sha256.h"
#include "client_server_model_msg_heads_macro.h"
#include "client_server_model_msg_struct.h"
#include "common_macro.h"
#include<functional>
#include <map>
#include<list>

/* auto time clock needs event mechnisam
 create thread to every client
how to calculate their online time ? store in structure, not in database because we only needs to know
their online time at this time*/
/*
 when logout, remove its corresponding thread_id and socketfd
 */


typedef struct _client_data
{
    //pthread_t threadID;
    struct sockaddr_in addr;
    time_t alive_time;
    //int sockid;
    
}client_data;

typedef struct _thread_arg
{
    int sock;
    struct sockaddr_in addr;
}thread_arg;

class Server
{
protected:
    
    std::map<int/*socketfd*/, client_data> clients_online;
    
    int Register(int sock,char *msg);
    int Login(int sock,char *msg);
    int Logout(int sock,char *msg);
    int Update_Policy(int sock,char *msg);
    int Update_Schedule(int sock, char *msg);
    int Update_Profile(int sock, char *msg);
    int Search_Friend(int sock, char *msg);
    int Add_Friend(int sock, char *msg);
    int Delete_Friend(int sock, char *msg);
    int Chat(int sock, char *msg);
    int Igore_Add_Request(int sock, char *msg);
    int Reject_Add_Request(int sock, char *msg);
    
    
    
public:
    
    TCPConnect serverTCPConnect;
    
    Server(int maxConnection = 2000, char *logname =(char *)"server.log");
    
    int Server_Start();
    
    int Server_Process_Users(void *arg);// we only needs to know user's sockid
    
    
    int Server_Restart();//reserved
    
    int Server_End();//send to all users that the server need to be updated
    
    
};

#endif /* server_hpp */











